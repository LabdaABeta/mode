# Mode - A modal golfing language

Mode is, as the name implies, a modal golfing language. It consists of three
separate character sets that mean different things. It is also strongly typed,
allowing for subdomain-specific operations to be encoded easily.

The target of Mode is not necessarily those problems that will be solved in
a handful of characters, but rather those problems that would normally be solved
in a few hundred characters. Thus it is designed to be easy to both read and
write.

## Codepage

As is often the case for golfing languages, Mode uses its own code page:

```
0123456789ABCDEFGHIJKLMNOPQRSTUV
WXYZabcdefghijklmnopqrstuvwxyz+/
-*|^&%<>()[]{}.,;:'"`~!@#$?_=\ ¶
0̲1̲2̲3̲4̲5̲6̲7̲8̲9̲A̲B̲C̲D̲E̲F̲G̲H̲I̲J̲K̲L̲M̲N̲O̲P̲Q̲R̲S̲T̲U̲V̲
W̲X̲Y̲Z̲a̲b̲c̲d̲e̲f̲g̲h̲i̲j̲k̲l̲m̲n̲o̲p̲q̲r̲s̲t̲u̲v̲w̲x̲y̲z̲+̲/̲
-̲*̲|̲^̲&̲%̲<̲>̲(̲)̲[̲]̲{̲}̲.̲,̲;̲:̲'̲"̲`̲~̲!̲@̲#̲$̲?̲_̲=̲\̲ ̲†
𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗𝐀𝐁𝐂𝐃𝐄𝐅𝐆𝐇𝐈𝐉𝐊𝐋𝐌𝐍𝐎𝐏𝐐𝐑𝐒𝐓𝐔𝐕
𝐖𝐗𝐘𝐙𝐚𝐛𝐜𝐝𝐞𝐟𝐠𝐡𝐢𝐣𝐤𝐥𝐦𝐧𝐨𝐩𝐪𝐫𝐬𝐭𝐮𝐯𝐰𝐱𝐲𝐳𝚺𝚷
```

However it is often best to view this as 3 sets of characters. The first set,
typeset normally, is used to define operations. The second set, typeset
underlined, is used to define control flow. The third set, typeset bold, is used
to define literal values.

When presenting mode code, it is reasonable to use available markdown support.
For instance `h̲e𝐥lo̲` could be typeset as <u>h</u>e**l**l<u>o</u>.

## Operations

The 'normal' text implements the actual operations of the language. Each symbol
represents a different operation. These operations are strongly typed based on
the input type and return type of the previous operation. The language of these
operations is a concatenative quotational language.

The quotational open symbol is the space while the quotational close symbol is
the newline. Unbalanced spaces/newlines will be balanced by appending the
necessary number of symbols to either the start or the end of the block.

## Control Flow

The 'underlined' text implements the control flow of the language. Everything
from branches to variables are defined using control flow. Additionally the
† character indicates the start of a line comment, which also consumes the
newline at the end of a line.

One of the important uses of control flow is to change how the normal operation
character set is interpreted. For example `name=̲expr` sets the variable `name`
to the result of evaluating `expr` rather than running `name` as code.

## Literals

The 'bold' text implements the literal values of the language. Their use varies
by context.

## Language

The input is compiled based entirely on the control flow symbols. The result of
compilation is Ada source code that can then be compiled to produce the final
executable.

The language that is compiled against can be described using EBNF as:


